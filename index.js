const express = require('express');
const app = express();
const axios = require('axios');
var mysql = require('mysql');
const { throws } = require('assert');
const { resourceUsage } = require('process');
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))


var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "user"
});

/*con.connect(function(err) {
    if (err) throw err;
    var sql = "SELECT users.name AS user, products.name AS favorite FROM customers JOIN products ON users.favorite_product = products.id";
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log(result);
    });
  }); */

app.get('/', async function (req, res) {
    try {
        var sql = "SELECT * FROM user ";
        con.query(sql, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    } catch (error) {
        res.send(error)
    }
})

app.post('/users', async function (req, res) {
    try {
        var firstname = req.query.firstname;
        var lastname = req.query.lastname;
        var email = req.query.email;
        var pseudo = req.query.pseudo;
        var date = new Date();
        var values = [[firstname, lastname, email, pseudo, date]]
        if(firstname && lastname && email && pseudo){
            await con.connect(function (err) {

                var sql = "INSERT INTO user (firstname, lastname,email,pseudo,createdAt) VALUES ?";
                con.query(sql, [values], function (err, result) {
                    if (err) throw err;
                    res.send(result)
                });
            });
        }else{
            res.send(null);
        }

       
    } catch (error) {
        res.send(error)
    }
})

app.get('/users/:userId', async function (req, res) {
    try {

        con.connect(function (err) {
            var id = req.params.userId
            var sql = "SELECT * FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.send(result);
            });

        });

    } catch (error) {
        res.send(error)
    }
})

app.get('/users', async function (req, res) {
    try {
        con.connect(function (err) {
            var limit = req.query.limit;
            var offset = req.query.offset;
            var order = req.query.order;
            var reverse = req.query.reverse;
            var filter = '';
            if(order){
                filter += 'ORDER BY '+order;
                if(reverse == 1){
                    filter += ' DESC '
                }
            }
            if(limit && offset){
                filter += "LIMIT "+offset+", "+limit
            }
            var sql = "SELECT * FROM user " +filter;
            con.query(sql, function (err, result) {
                if (err) throw err;
                res.send(result);
            });
        });
    } catch (error) {
        res.send(error)
    }
})

app.delete('/users/:userId', async function (req, res) {
    try {
        con.connect(function (err) {
            var id = req.params.userId
            var sql = "SELECT * FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.send(result);
            });

            var sql = "DELETE FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
            });
        });
    } catch (error) {
        res.send(error)
    }
})

app.put('/users/:userId', async function (req, res) {
    try {
        con.connect(function (err) {

            var firstname = req.query.firstname ? req.query.firstname : 'default';
            var lastname = req.query.lastname ? req.query.lastname : 'default';
            var email = req.query.email ? req.query.email : 'default@orange.com';
            var pseudo = req.query.pseudo ? req.query.pseudo : 'default';
            var date = new Date();
            var id = req.params.userId;
            if(firstname && lastname && email && pseudo){
                var sql = "UPDATE user SET firstname = ?, lastname = ?, email = ?, pseudo = ?, updatedAt = ? WHERE id = ?";
                con.query(sql, [firstname, lastname, email, pseudo, date, id], function (err, result) {
                    if (err) throw err;
                });
                
    
                var sql = "SELECT * FROM user WHERE id = ?";
                con.query(sql, [id], function (err, result) {
                    if (err) throw err;
                    console.log(result);
                    res.send(result);
                });
            }else{
                res.send(null);
            }


        });

    } catch (error) {
        res.send(error)
    }
});

app.post('/users/:userId/pokemons', async function (req, res) {
    try {
        var pokemonName = "https://pokeapi.co/api/v2/pokemon/"+req.query.pokemonName;

        if(pokemonName ){
            await con.connect(function (err) {
                var values = [[pokemonName,req.params.userId,req.query.pokemonName]]
                var sql = "INSERT INTO pokemon (url_pokemon,id_user,nom) VALUES ?";
                con.query(sql, [values], function (err, result) {
                    if (err) throw err;
                    res.send(result)
                });
            });
        }else{
            res.send(null);
        }


       
    } catch (error) {
        res.send(error)
    }
});


app.get('/pokemons/:idPokemon', async function (req, res) {
    try {

        con.connect(function (err) {
            var id = req.params.idPokemon
            var sql = "SELECT url_pokemon FROM pokemon WHERE id_pokemon = ?";
            con.query(sql, [id], async function (err, result) {
                if (err) throw err;

                const {data} = await axios.get(result[0].url_pokemon)
                res.send(data)
            });

        });
       
    } catch (error) {
        res.send(error)
    }
});



app.delete('/pokemons/:idPokemon', async function (req, res) {
    try {
        con.connect(function (err) {
            var id = req.params.idPokemon
            var sql = "SELECT * FROM pokemon WHERE id_pokemon = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.send(result);
            });

            var sql = "DELETE FROM pokemon WHERE id_pokemon = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
            });
        });
    } catch (error) {
        res.send(error)
    }
})


app.get('/users/:idUsers/pokemons', async function (req, res) {
    try {

        con.connect(function (err) {
            var idUser =  req.params.idUsers
            var sql = "SELECT nom FROM pokemon WHERE id_user = ? ";
            con.query(sql, [idUser], async function (err, result) {
                if (err) throw err;
                res.send(result)
            });

        });
       
    } catch (error) {
        res.send(error)
    }
});

app.put('/pokemons/:pokemonId', async function (req, res) {
    try {
        con.connect(function (err) {

            var pokemonName = req.query.pokemonName
            var id = req.params.pokemonId;
            if(pokemonName){
                var pokemonRoute = "https://pokeapi.co/api/v2/pokemon/"+pokemonName;
                var sql = "UPDATE pokemon SET url_pokemon = ?, nom = ? WHERE id_pokemon = ?";
                con.query(sql, [pokemonRoute, pokemonName, id], function (err, result) {
                    if (err) throw err;
                });
                
    
                var sql = "SELECT * FROM user WHERE id = ?";
                con.query(sql, [id], function (err, result) {
                    if (err) throw err;
                    console.log(result);
                    res.send(result);
                });
            }else{
                res.send(null);
            }
            

        });

    } catch (error) {
        res.send(error)
    }
});


app.listen(3001)






//https://jsonplaceholder.typicode.com/


